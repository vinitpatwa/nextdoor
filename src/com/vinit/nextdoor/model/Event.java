package com.vinit.nextdoor.model;


import java.io.Serializable;


public class Event implements Serializable{

	String title;
	String date;
	boolean allow_rsvp;
	String description; 
	String start_time;
	String end_time;
	
	public Event(String title, String date, boolean allow_rsvp,
			String description, String start_time, String end_time) {
		super();
		this.title =title;
		this.date = date;
		this.allow_rsvp = allow_rsvp;
		this.description = description;
		this.start_time = start_time;
		this.end_time = end_time;
	}


	public String getTitle() {
		return title;
	}
	public String getDate() {
		return date;
	}
	public boolean getAllow_rsvp() {
		return allow_rsvp;
	}
	public String getDescription() {
		return description;
	}
	public String getStart_time() {
		return start_time;
	}
	public String getEnd_time() {
		return end_time;
	}
	
}