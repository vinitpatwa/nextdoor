package com.vinit.nextdoor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.vinit.nextdoor.fragment.ViewEventFragment;
import com.vinit.nextdoor.model.Event;

public class ViewEventActivity extends FragmentActivity {
	
	TextView tv_title;
	TextView tv_month;
	TextView tv_date;
	TextView tv_date_full;
	TextView tv_view_event_description;
	TextView tv_view_event_details_dnt_info;
	Event rec_event;
	Calendar cal;
	 String date_str;
	 String time ;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_event);
		setupFragment();
		
		 Bundle bundle = getIntent().getExtras();
          rec_event = (Event) bundle.getSerializable("event");
         Log.d("DEBUG4", rec_event.getTitle());
         
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onPostCreate(savedInstanceState);
		
		
		 tv_title = (TextView) findViewById(R.id.tv_title);
		 tv_month = (TextView) findViewById(R.id.tv_month);
		 tv_date = (TextView) findViewById(R.id.tv_date);
		 tv_date_full = (TextView) findViewById(R.id.tv_date_full);
		 tv_view_event_description = (TextView) findViewById(R.id.tv_view_event_description);
		 tv_view_event_details_dnt_info = (TextView) findViewById(R.id.tv_view_event_details_dnt_info);
		 

		 Log.d("DEBUG4", rec_event.getDate().toString());
		  date_str = rec_event.getDate().toString();
		  time = rec_event.getStart_time().toString();
		 Log.d("DEBUG4", "time".concat(time));
		 
		 
		 SimpleDateFormat sdf= new SimpleDateFormat("MM-d-yyyy");
		 Date dt=null;
		try {
			dt = sdf.parse(date_str);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 Log.d("DEBUG4", dt.toString());
		 
		 tv_title.setText(rec_event.getTitle());
		 tv_view_event_description.setText(rec_event.getDescription());
	
		 tv_date_full.setText(dt.toString());
		 
		   cal = Calendar.getInstance();
		    cal.setTime(dt);
		    int year = cal.get(Calendar.YEAR);
		    int month = cal.get(Calendar.MONTH);
		    int day = cal.get(Calendar.DAY_OF_MONTH);
		    
		 tv_month.setText(new SimpleDateFormat("MMM").format(cal.getTime()));
		 Log.d("DEBUG4", new SimpleDateFormat("MMM").format(cal.getTime()));
		 String dnt = new SimpleDateFormat("EEE MMM d yyyy").format(cal.getTime()).concat(" "+time);
		 
		 tv_date_full.setText(new SimpleDateFormat("EEE MMM d yyyy").format(cal.getTime()).concat(" "+time));
		 tv_view_event_details_dnt_info.setText(new SimpleDateFormat("EEE MMM d yyyy").format(cal.getTime()).concat(" "+time));
		 tv_date.setText(Integer.toString(day));

		 

		 
	}

	private void setupFragment() {
		FragmentManager manager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fts = manager.beginTransaction();
        //Adding CreateEventFragment to FragmentTransaction
        ViewEventFragment cEFrag = new ViewEventFragment(); 
        fts.add(R.id.fragment_container, cEFrag);
        fts.commit();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view_event, menu);
		return true;
	}
	
	
	public static Calendar DateToCalendar(Date date){ 
		  Calendar cal = Calendar.getInstance();
		  cal.setTime(date);
		  return cal;
		}
	
	
	public Calendar getCalendar(String dateAndTime){
		SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM d yyyy hh:mm aa");
		String dateInString = dateAndTime;
		Date date = null;
		try {
			Log.d("DEBUG4","in TRY".concat(dateAndTime));
			date = sdf.parse(dateInString);
		} catch (java.text.ParseException e) {
			Log.d("DEBUG4","in CATCH");
			e.printStackTrace();
		}
		
		if(date == null){
			Log.d("DEBUG4", "date is null");
		}
		Calendar beginTime = DateToCalendar(date);
		
		return beginTime;
	}
	
	@SuppressLint("NewApi")
	public void addToCalendar(View v){
		
		String event_title = tv_title.getText().toString();
		Log.d("DEBUG4", "event_title".concat(event_title));
		

		TextView tv_date_n_time = (TextView) findViewById(R.id.tv_view_event_details_dnt_info);
		String event_time = tv_date_n_time.getText().toString();
		Log.d("DEBUG4", "event_time".concat(event_time));
		
		Calendar beginTime = this.getCalendar(event_time);
		
		long endTime = beginTime .getTimeInMillis()+1000*30*60;

		TextView tv_event_description = (TextView) findViewById(R.id.tv_view_event_description);
		String event_description = tv_event_description.getText().toString();
		Log.d("DEBUG4", "event_description".concat(event_description));
		
//		String beginTimeString
		if (Build.VERSION.SDK_INT >= 14) {

			Intent intent = new Intent(Intent.ACTION_INSERT)
	        .setData(Events.CONTENT_URI)
	        .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime .getTimeInMillis())
	        .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime)
	        .putExtra(Events.TITLE, event_title)
	        .putExtra(Events.DESCRIPTION, event_description)
	        .putExtra(Events.AVAILABILITY, Events.AVAILABILITY_BUSY);
	         startActivity(intent);
	}

	    else {
	        Calendar cal = Calendar.getInstance();              
	        Intent intent = new Intent(Intent.ACTION_EDIT);
	        intent.setType("vnd.android.cursor.item/event");
	        intent.putExtra("beginTime", beginTime .getTimeInMillis());
	        intent.putExtra("allDay", false);
	        intent.putExtra("endTime", endTime);
	        intent.putExtra("title", event_title);
	        startActivity(intent);
	        }
	}

}
