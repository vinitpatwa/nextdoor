package com.vinit.nextdoor;


import java.util.Calendar;

import android.annotation.TargetApi;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.vinit.nextdoor.R.string;
import com.vinit.nextdoor.fragment.CreateEventFragment;
import com.vinit.nextdoor.fragment.DateDialogFragment;
import com.vinit.nextdoor.fragment.TimePickerFragment;
import com.vinit.nextdoor.model.Event;



public class CreateEventActivity extends FragmentActivity implements com.vinit.nextdoor.fragment.TimePickerFragment.TimePickedListener {

	Calendar now;
	DateDialogFragment frag;
	Button  bt_create_event_date;
	Button	bt_create_event_time;
	Button	bt_create_event_end_time;
	EditText et_event_title;
	EditText et_event_description;
	CheckBox chk_rsvp;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_event);
		//Add a fragment to include Create Event Page
		setupFragment();
		
		
		
		
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onPostCreate(savedInstanceState);
		
		bt_create_event_date = (Button) findViewById(R.id.bt_create_event_date);
		bt_create_event_time = (Button) findViewById(R.id.bt_create_event_time);
		bt_create_event_end_time = (Button) findViewById(R.id.bt_create_event_end_time);
		et_event_title = (EditText) findViewById(R.id.et_event_title);
		et_event_description = (EditText) findViewById(R.id.et_event_description);
		chk_rsvp = (CheckBox) findViewById(R.id.chk_rsvp);
		
		now = Calendar.getInstance();
		if(now == null){
			Log.d("DEBUG4", "now is null");
		}
		if(bt_create_event_date == null){
			Log.d("DEBUG4", "bt_create_event_date is null");
		}
		
		bt_create_event_date.setText(String.valueOf(now.get(Calendar.MONTH)+1)+"-"+String.valueOf(now.get(Calendar.DAY_OF_MONTH))+"-"+String.valueOf(now.get(Calendar.YEAR)));
		bt_create_event_date.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				showDialog();        
			}
		});

		bt_create_event_time.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				showTimeDialog();
			}
		});
		
	}
	
	private void setupFragment() {
		FragmentManager manager = getSupportFragmentManager();
		android.support.v4.app.FragmentTransaction fts = manager.beginTransaction();
		//Adding CreateEventFragment to FragmentTransaction
		CreateEventFragment cEFrag = new CreateEventFragment(); 
		fts.add(R.id.fragment_container, cEFrag);
		fts.commit();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.create_event, menu);
		return true;
	}
	public void onPost(View v){


		if(et_event_title.getText().toString().equals("") || et_event_title.getText().toString() == null){
			Toast.makeText(this,"Please fill Title", Toast.LENGTH_SHORT).show();
			return;
		}
		
		if(bt_create_event_time.getText().toString().equals("CHANGE TIME")){
			Toast.makeText(this,"Please Select Time", Toast.LENGTH_SHORT).show();
			return;
		}
		
		if(et_event_description.getText().toString().equals("") || et_event_description.getText().toString() == null){
			Toast.makeText(this,"Please fill in description", Toast.LENGTH_SHORT).show();
			return;
		}
				
		Event event = new Event(et_event_title.getText().toString(), bt_create_event_date.getText().toString(), chk_rsvp.isChecked(), et_event_description.getText().toString(), bt_create_event_time.getText().toString(),  bt_create_event_end_time.getText().toString());
		
		
		Intent i = new Intent(this, ViewEventActivity.class);
		i.putExtra("event",event);
		startActivity(i);

	}

	public void onCancel(View v){

		//Clear Everything

	}
	
	public interface DateDialogFragmentListener{
		//this interface is a listener between the Date Dialog fragment and the activity to update the buttons date
		public void updateChangedDate(int year, int month, int day);
	}


	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void showTimeDialog() {
		// show the time picker dialog
		TimePickerFragment newFragment = new TimePickerFragment();
		newFragment.show(getFragmentManager(), "timePicker");

	}

	@Override
	public void onTimePicked(Calendar time) {
		// TODO Auto-generated method stub
		int hour = time.get(Calendar.HOUR);
		int am_pm = time.get(Calendar.AM_PM);
		int minute = time.get(Calendar.MINUTE);
		String timeInAmPm;
		if(am_pm == 1){
				timeInAmPm = "PM";
		}else
		{
			timeInAmPm = "AM";
		}
		bt_create_event_time.setText(Integer.toString(hour).concat(":").concat(String.format("%02d",minute).concat(" ").concat(timeInAmPm)));
	}
	

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void showDialog() {
		FragmentTransaction ft = getFragmentManager().beginTransaction(); //get the fragment
		frag = DateDialogFragment.newInstance(this, new DateDialogFragmentListener(){
			public void updateChangedDate(int year, int month, int day){
				
				bt_create_event_date.setText(String.valueOf(month+1)+"-"+String.valueOf(day)+"-"+String.valueOf(year));
				now.set(year, month, day);
			}
		}, now);

		frag.show(ft, "DateDialogFragment");

	}
}
